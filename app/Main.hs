{- | Example app.

Demonstrates user registration, authentication and session functionality.

The app needs to do the following things:

1. At the beginning of the program:
    - Create an sqlite database connection pool
    - Initialize the sqlite database tables with @dbMigrations@ using the connection pool
    - Initialize the session store with @initSessionStore@ and get back a SessionStore

2. Call @router@ from your routing so it will take care of the user registration, login and logout pages

3. Use @loginOrLogout@ to welcome the user and provide links for the registration, login or logout pages

4. Use @withLogin@ for @ActionM@s that require a logged-in user.

-}

{-# language OverloadedStrings #-}

module Main where

import qualified Control.Monad.Logger as Log
import qualified Web.Scotty as S
import qualified Data.Text.Lazy as TL
import qualified Database.Persist.Sqlite as PSqlite3
import Control.Monad.IO.Class (liftIO)
import qualified Lucid as H

import qualified Web.Scotty.Sqlite.Users as Users

main :: IO ()
main = do
  pool <- do                           -- (1.1)
    Log.runStderrLoggingT $ PSqlite3.createSqlitePool "file:/tmp/my-scotty-users.db" 1
  liftIO $ Users.dbMigrations pool     -- (1.2)
  ses <- Users.initSessionStore        -- (1.3)
  let sinfo = Users.StateInfo pool ses Users.Development -- Use Production for secure only cookies
  S.scotty 8080 (myApp sinfo)

myApp :: Users.StateInfo -> S.ScottyM ()
myApp sinfo = do
  Users.router sinfo template          -- (2)

  S.get "/" $ do
    io <- Users.loginOrLogout sinfo    -- (3)
    S.html $
      H.renderText $
        template
          ("Bulletin board")
          ( do
            io
            H.p_ $ H.a_ [ H.href_ "/users" ] "Users zone"
          )

  S.get "/users" $                     -- (4)
    Users.withLogin sinfo $ \me -> do
      users <- liftIO $ Users.getAllUsers sinfo
      S.html $
        H.renderText $
          template "User zone" $ do
            H.p_ $ H.toHtml $ "You are: " <> Users.loginDisplayName (Users.getLoginDesc me)
            H.p_ $ "And these are everyone:"
            H.ul_ $
              mapM_ (H.li_ . H.toHtml . Users.loginDisplayName . Users.getLoginDesc) users
      

type Html = H.Html ()

template :: TL.Text -> Html -> Html
template title content =
  H.doctypehtml_ $ do
    H.head_ $ do
      H.meta_ [ H.charset_ "utf-8" ]
      H.title_ (H.toHtml title)
      H.link_ [ H.rel_ "stylesheet", H.type_ "text/css", H.href_ "/style.css"  ]
    H.body_ $ do
      content
