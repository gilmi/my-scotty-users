{- | Session management.

The one useful function here is @initSessionStore@
which is already exported from @Web.Scotty.Sqlite.Users@.

-}

{-# language OverloadedStrings #-}

module Web.Scotty.Sqlite.Users.Session
  ( SessionStore
  , initSessionStore
  , newSession
  , getSession
  , deleteSession
  )
where

import Control.Monad (join)
import qualified Data.Map as Map
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.Lazy.Encoding as TL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Time.Clock as Time
import qualified Data.Password.Bcrypt as Bcrypt

import Control.Monad.IO.Class (liftIO)
import qualified Control.Concurrent.STM as STM
import qualified Web.Scotty as S
import qualified Web.ClientSession as CS
import qualified Web.Cookie as C
import qualified Data.Binary.Builder as Binary

import Web.Scotty.Sqlite.Users.Model
import Web.Scotty.Sqlite.Users.Common


-------------------
-- Session Store --
-------------------

-- | Run this at the beginning of the program
--   To create a session store.
--   The session store is only alive for the duration of the program.
initSessionStore :: IO SessionStore
initSessionStore = do
  SessionStore
    <$> STM.newTVarIO mempty
    <*> CS.getDefaultKey

insertLoginId :: LoginId -> BS.ByteString -> StateInfo -> IO ()
insertLoginId uid bs sinfo = do
  STM.atomically $ STM.modifyTVar' (sessionTVar (siSessions sinfo)) (Map.insert bs uid)

deleteLoginId :: LoginId -> StateInfo -> IO ()
deleteLoginId uid sinfo = do
  STM.atomically $ STM.modifyTVar' (sessionTVar (siSessions sinfo)) (Map.filter (/=uid))

getSessionStore :: StateInfo -> IO (Map.Map BS.ByteString LoginId)
getSessionStore = STM.readTVarIO . sessionTVar . siSessions

--------------
-- Sessions --
--------------

newSession :: StateInfo -> LoginId -> Bcrypt.PasswordHash Bcrypt.Bcrypt -> S.ActionM ()
newSession sinfo uid hash = do
  let
    tok = T.encodeUtf8 $ T.pack $ show uid <> show hash
    makeCookie =
      case siMode sinfo of
        Production -> mkCookie
        Development -> mkInsecureCookie
  cookie <-
    liftIO $
      BSL.fromStrict <$>
        encryptCookieIO
          (sessionKey (siSessions sinfo))
          (makeCookie "logged_in" tok)
  S.setHeader "Set-Cookie" (TL.decodeUtf8 cookie)
  liftIO $ insertLoginId uid tok sinfo

-- | Try to get the currently logged-in user
getSession :: StateInfo -> S.ActionM (Maybe LoginId)
getSession sinfo = do
  bytes <- fmap TL.encodeUtf8 <$> S.header "Cookie"
  cookie <- fmap join . liftIO $ traverse (decryptCookieIO (sessionKey (siSessions sinfo))) bytes
  case cookie of
    Just ("logged_in", tok) -> do
      sessions <- liftIO $ getSessionStore sinfo
      pure $ tok `Map.lookup` sessions
    _ ->
      pure Nothing

deleteSession :: StateInfo -> LoginId -> S.ActionM ()
deleteSession sinfo uid =
  liftIO $ deleteLoginId uid sinfo

----------------------------
-- Cookie Encrypt/Decrypt --
----------------------------

encryptCookieIO :: CS.Key -> C.SetCookie -> IO BS.ByteString
encryptCookieIO key cookie = do
  v <- CS.encryptIO key $ C.setCookieValue cookie
  pure . BSL.toStrict . Binary.toLazyByteString . C.renderSetCookie $ cookie
    { C.setCookieValue = v
    }

decryptCookieIO :: CS.Key -> BSL.ByteString -> IO (Maybe (BS.ByteString, BS.ByteString))
decryptCookieIO key bs =
  let
    (name, value) = getCookieKV (BSL.toStrict bs)
  in
    case CS.decrypt key value of
      Nothing ->
        pure Nothing
      Just value' ->
        pure $ Just (name, value')

-------------
-- Cookies --
-------------


mkCookie :: BS.ByteString -> BS.ByteString -> C.SetCookie
mkCookie name value =
  C.defaultSetCookie
    { C.setCookieName = name
    , C.setCookieValue = value
    , C.setCookiePath = Just "/"
    , C.setCookieSecure = True
    , C.setCookieHttpOnly = True
    , C.setCookieSameSite = Just C.sameSiteStrict
    , C.setCookieMaxAge = Just sixtyDays
    }

mkInsecureCookie :: BS.ByteString -> BS.ByteString -> C.SetCookie
mkInsecureCookie name value =
  (mkCookie name value)
    { C.setCookieSecure = False
    }


-- utils --

sixtyDays :: Time.DiffTime
sixtyDays = Time.secondsToDiffTime (60 * 60 * 24 * 60)

getCookieKV :: BS.ByteString -> (BS.ByteString, BS.ByteString)
getCookieKV bytes =
  let
    cookie = C.parseSetCookie bytes
  in
    (C.setCookieName cookie, C.setCookieValue cookie)

