{- | Create new password actions.

Internal API for the library to handle the http, forms and actions.

-}

{-# language OverloadedStrings #-}

module Web.Scotty.Sqlite.Users.ChangePassword where

import qualified Data.Text as T
import qualified Data.Text.Lazy as TL

import Control.Monad.IO.Class (liftIO)
import qualified Data.Password.Bcrypt as Bcrypt
import qualified Lucid as H
import qualified Web.Scotty as S

import qualified Web.Scotty.Sqlite.Users.DbAccess as DB
import Web.Scotty.Sqlite.Users.Session
import Web.Scotty.Sqlite.Users.Common
import Web.Scotty.Sqlite.Users.Model

-------------
-- Routing --
-------------

router :: StateInfo -> HtmlTemplate -> S.ScottyM ()
router sinfo template = do
  -- A page for creating a new post
  S.get "/change-password" $ do
    runFinalAction =<< changePasswordGetAction sinfo template "/change-password"

  -- A request to create a new user
  S.post "/change-password" $ do
    runFinalAction =<< changePasswordPostAction sinfo template "/change-password"

------

type Result = Result' (S.ActionM ())

runFinalAction :: Result' a -> a
runFinalAction result =
  case result of
    NotLoggedIn a -> a
    ChangePasswordFormServe a -> a
    ChangePasswordSuccess a -> a

data Result' a
  = NotLoggedIn a
  | ChangePasswordFormServe a
  | ChangePasswordSuccess a

-- | A page for change a password
changePasswordGetAction :: StateInfo -> HtmlTemplate -> T.Text -> S.ActionM Result
changePasswordGetAction sinfo template actionPath = do
  mses <- getSession sinfo
  case mses of
    Just{} ->
      serveChangePasswordForm template actionPath noChangePasswordErrors "" ""
    Nothing ->
      pure $ NotLoggedIn $ S.redirect "/"

-- | A request to change a password
changePasswordPostAction :: StateInfo -> HtmlTemplate -> T.Text -> S.ActionM Result
changePasswordPostAction sinfo template actionPath = do
  mses <- getSession sinfo
  case mses of
    Just uid -> do
      password <- S.param "password"
      confirm <- S.param "confirm"
      submitChangeForm sinfo template actionPath uid password confirm
    Nothing ->
      pure $ NotLoggedIn $ S.redirect "/"


-------------
-- Actions --
-------------

serveChangePasswordForm :: HtmlTemplate -> T.Text -> ChangePasswordErrors -> TL.Text -> TL.Text -> S.ActionM Result
serveChangePasswordForm template actionPath err pass confirm = do
  pure $ ChangePasswordFormServe $ S.html $
    H.renderText $
      template
        ("Scotty.Sqlite board - register a new account")
        (changePasswordHtml err actionPath pass confirm)

submitChangeForm :: StateInfo -> HtmlTemplate -> T.Text -> LoginId -> TL.Text -> TL.Text -> S.ActionM Result
submitChangeForm sinfo template actionPath uid pass confirm = do
  let errs = validateChangePassword pass confirm
  if hasChangePasswordErrors errs
    then
      serveChangePasswordForm template actionPath errs pass confirm
    else do
      deleteSession sinfo uid
      liftIO $ changePass sinfo uid (TL.toStrict pass)
      pure $ ChangePasswordSuccess $ S.redirect "/"

changePass :: StateInfo -> LoginId -> T.Text -> IO ()
changePass sinfo uid password = do
  hash <- liftIO $ Bcrypt.hashPassword $ Bcrypt.mkPassword password
  DB.changePassword sinfo uid hash

----------
-- Html --
----------


changePasswordHtml :: ChangePasswordErrors -> T.Text -> TL.Text -> TL.Text -> Html
changePasswordHtml errs actionPath password confirm = do
  H.h2_ [ H.class_ "register-title" ] "Change password"
  H.form_
    [ H.method_ "post"
    , H.action_ actionPath
    , H.class_ "new-user"
    ]
    ( do
      applyMaybe (cpePassword errs) (H.p_ [ H.class_ "error" ] . H.toHtml) 
      H.p_ $
        H.input_ $
          [ H.type_ "password"
          , H.required_ "true"
          , H.name_ "password"
          , H.placeholder_ "New password..."
          , H.value_ (TL.toStrict password)
          , H.autofocus_
          ]
      applyMaybe (cpeConfirm errs) (H.p_ [ H.class_ "error" ] . H.toHtml) 
      H.p_ $
        H.input_ $
          [ H.type_ "password"
          , H.required_ "true"
          , H.name_ "confirm"
          , H.placeholder_ "Confirm new password..."
          , H.value_ (TL.toStrict confirm)
          , H.autofocus_
          ]
      H.p_ $
        H.input_
          [ H.type_ "submit"
          , H.value_ "Change"
          , H.class_ "submit-button"
          ]
    )




----------------
-- Validation --
----------------

data ErrorPart
  = Password
  | Confirm

data ChangePasswordErrors
  = ChangePasswordErrors
    { cpePassword :: Maybe TL.Text
    , cpeConfirm :: Maybe TL.Text
    }
    deriving (Show, Eq)


noChangePasswordErrors :: ChangePasswordErrors
noChangePasswordErrors = ChangePasswordErrors Nothing Nothing

hasChangePasswordErrors :: ChangePasswordErrors -> Bool
hasChangePasswordErrors errs =
  errs /= noChangePasswordErrors

validateChangePassword :: TL.Text -> TL.Text -> ChangePasswordErrors
validateChangePassword password confirm = do
  ChangePasswordErrors
    { cpePassword =
      if TL.length password >= 8 && TL.length password <= 18
        then
          Nothing
        else
          Just "Passwords must be at least 8 character long and at most 18 characters long"
    , cpeConfirm =
      if password /= confirm
        then
          Just "Password and confirmation does not match."
        else
          Nothing
    }
