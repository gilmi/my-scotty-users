{- | Create new user actions.

Internal API for the library to handle the http, forms and actions.

-}

{-# language OverloadedStrings #-}
{-# language MultiWayIf #-}

module Web.Scotty.Sqlite.Users.NewUser where

import Data.Maybe (isJust)
import Data.Char (isDigit, isLetter)
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL

import Control.Monad.IO.Class (liftIO)
import qualified Data.Password.Bcrypt as Bcrypt
import qualified Lucid as H
import qualified Web.Scotty as S

import qualified Web.Scotty.Sqlite.Users.DbAccess as DB
import Web.Scotty.Sqlite.Users.Session
import Web.Scotty.Sqlite.Users.Common


-------------
-- Routing --
-------------

router :: StateInfo -> HtmlTemplate -> S.ScottyM ()
router sinfo template = do
  -- A page for creating a new post
  S.get "/register" $ do
    runFinalAction =<< registerGetAction sinfo template "/register"

  -- A request to create a new user
  S.post "/register" $ do
    runFinalAction =<< registerPostAction sinfo template "/register"

------

type Result = Result' (S.ActionM ())

runFinalAction :: Result' a -> a
runFinalAction result =
  case result of
    AlreadyLoggedIn a -> a
    RegistrationFormServe a -> a
    RegistrationSuccess a -> a

data Result' a
  = AlreadyLoggedIn a
  | RegistrationFormServe a
  | RegistrationSuccess a

-- | A page for creating a new post
registerGetAction :: StateInfo -> HtmlTemplate -> T.Text -> S.ActionM Result
registerGetAction sinfo template actionPath = do
  mses <- getSession sinfo
  case mses of
    Nothing ->
      serveNewUserForm template actionPath noNewUserErrors "" "" ""
    _ ->
      pure $ AlreadyLoggedIn $ S.redirect "/"

-- | A request to create a new user
registerPostAction :: StateInfo -> HtmlTemplate -> T.Text -> S.ActionM Result
registerPostAction sinfo template actionPath = do
  mses <- getSession sinfo
  case mses of
    Nothing -> do
      username <- S.param "username"
      password <- S.param "password"
      confirm <- S.param "confirm"
      submitNewUserForm sinfo template actionPath username password confirm
    _ ->
      pure $ AlreadyLoggedIn $ S.redirect "/"

-------------
-- Actions --
-------------

serveNewUserForm :: HtmlTemplate -> T.Text -> NewUserErrors -> TL.Text -> TL.Text -> TL.Text -> S.ActionM Result
serveNewUserForm template actionPath err username pass confirm = do
  pure $ RegistrationFormServe $ S.html $
    H.renderText $
      template
        ("Scotty.Sqlite board - register a new account")
        (newUserHtml err actionPath username pass confirm)

submitNewUserForm :: StateInfo -> HtmlTemplate -> T.Text -> TL.Text -> TL.Text -> TL.Text -> S.ActionM Result
submitNewUserForm sinfo template actionPath username pass confirm = do
  errs <- liftIO $ validateNewUser sinfo username pass confirm
  if hasNewUserErrors errs
    then
      serveNewUserForm template actionPath errs username pass confirm
    else do
      hash <- liftIO $ Bcrypt.hashPassword $ Bcrypt.mkPassword (TL.toStrict pass)
      ukey <- liftIO $ DB.insertUser sinfo username hash username
      newSession sinfo ukey hash
      pure $ RegistrationSuccess $ S.redirect "/"

----------
-- Html --
----------

newUserHtml :: NewUserErrors -> T.Text -> TL.Text -> TL.Text -> TL.Text -> Html
newUserHtml errs actionPath username password confirm = do
  H.h2_ [ H.class_ "register-title" ] "Register"
  H.form_
    [ H.method_ "post"
    , H.action_ actionPath
    , H.class_ "new-user"
    ]
    ( do
      applyMaybe (nueUsername errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
      H.p_ $
        H.input_ $
          [ H.type_ "text"
          , H.required_ "true"
          , H.name_ "username"
          , H.placeholder_ "Username..."
          , H.value_ (TL.toStrict username)
          ] <> newUserFocus Username errs
      applyMaybe (nuePassword errs) (H.p_ [ H.class_ "error" ] . H.toHtml) 
      H.p_ $
        H.input_ $
          [ H.type_ "password"
          , H.required_ "true"
          , H.name_ "password"
          , H.placeholder_ "Password..."
          , H.value_ (TL.toStrict password)
          ] <> newUserFocus Password errs
      applyMaybe (nueConfirm errs) (H.p_ [ H.class_ "error" ] . H.toHtml) 
      H.p_ $
        H.input_ $
          [ H.type_ "password"
          , H.required_ "true"
          , H.name_ "confirm"
          , H.placeholder_ "Confirm password..."
          , H.value_ (TL.toStrict confirm)
          ] <> newUserFocus Confirm errs
      H.p_ $
        H.input_
          [ H.type_ "submit"
          , H.value_ "Submit"
          , H.class_ "submit-button"
          ]
    )

newUserFocus :: ErrorPart -> NewUserErrors -> [H.Attribute]
newUserFocus part errs =
  case part of
    Username
      | not (hasNewUserErrors errs) || isJust (nueUsername errs) ->
        [H.autofocus_]
    Password
      | not (isJust (nueUsername errs)) && isJust (nuePassword errs) ->
        [H.autofocus_]
    Confirm
      | not (isJust (nueUsername errs))
        && not (isJust (nuePassword errs))
        && isJust (nueConfirm errs) ->
        [H.autofocus_]
    _ -> []


----------------
-- Validation --
----------------

data ErrorPart
  = Username
  | Password
  | Confirm

data NewUserErrors
  = NewUserErrors
    { nueUsername :: Maybe TL.Text
    , nuePassword :: Maybe TL.Text
    , nueConfirm :: Maybe TL.Text
    }
    deriving (Show, Eq)


noNewUserErrors :: NewUserErrors
noNewUserErrors = NewUserErrors Nothing Nothing Nothing

hasNewUserErrors :: NewUserErrors -> Bool
hasNewUserErrors errs =
  errs /= noNewUserErrors

validateNewUser :: StateInfo -> TL.Text -> TL.Text -> TL.Text -> IO NewUserErrors
validateNewUser sinfo username password confirm = do
  login <- DB.getLoginByUsername sinfo username
  pure $ NewUserErrors
    { nueUsername =
      if
        | isJust login ->
          Just "Username is already taken"
        | TL.length username < 2 || TL.length username > 20 ->
          Just "Username must be at least 2 characters long and at most 20 characters long"
        | not $ all (\c -> isDigit c || isLetter c || c `elem` ['-','_']) (TL.unpack username) ->
          Just "Usernames can only contain letters, digits, underscores or dashes"
        | otherwise ->
          Nothing
    , nuePassword =
      if TL.length password >= 8 && TL.length password <= 18
        then
          Nothing
        else
          Just "Passwords must be at least 8 character long and at most 18 characters long"
    , nueConfirm =
      if password /= confirm
        then
          Just "Password and confirmation does not match."
        else
          Nothing
    }
