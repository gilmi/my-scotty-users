{- | Users model

-}

{-# language TemplateHaskell #-}
{-# language QuasiQuotes #-}
{-# language TypeFamilies #-}
{-# language GADTs #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}


module Web.Scotty.Sqlite.Users.Model where

import Data.Int (Int64)
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Database.Persist as P
import qualified Database.Persist.Sql as P
import Database.Persist.TH

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Login
    username TL.Text
    passwordHash T.Text
    displayName TL.Text
    Username username
    deriving Show

|]

type Logins = [P.Entity Login]
type Username = TL.Text
type Displayname = TL.Text

-- | Extract LoginId from a LoginIn/Login pair.
getLoginId :: P.Entity Login -> LoginId
getLoginId = P.entityKey

-- | Extract Login from a LoginIn/Login pair.
getLoginDesc :: P.Entity Login -> Login
getLoginDesc = P.entityVal

-- | Convert an integer id to a LoginId.
--   Useful for routing.
paramToLoginId :: Int64 -> LoginId
paramToLoginId = P.toSqlKey
