module Web.Scotty.Sqlite.Users.Common where

import qualified Data.ByteString as BS
import qualified Data.Text.Lazy as TL
import qualified Lucid as H
import qualified Control.Concurrent.STM as STM
import qualified Web.ClientSession as CS
import qualified Database.Persist.Sqlite as PSqlite3
import qualified Data.Map as Map

import Web.Scotty.Sqlite.Users.Model

data StateInfo
  = StateInfo
    { siPool :: Pool
    , siSessions :: SessionStore
    , siMode :: Mode
    }

data Mode
  = Development
  | Production
  deriving (Show, Eq)

data SessionStore
  = SessionStore
  { sessionTVar :: STM.TVar (Map.Map BS.ByteString LoginId)
  , sessionKey :: CS.Key
  }

type Pool = PSqlite3.ConnectionPool

type Html = H.Html ()
type HtmlTemplate = TL.Text -> Html -> Html

applyMaybe :: Applicative m => Maybe a -> (a -> m ()) -> m ()
applyMaybe m f =
  case m of
    Nothing -> pure ()
    Just x -> f x
