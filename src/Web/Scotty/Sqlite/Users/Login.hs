{- | Login/Logout users

Internal API for the library to handle the http, forms and actions.

-}

{-# language OverloadedStrings #-}

module Web.Scotty.Sqlite.Users.Login where


import Control.Monad.IO.Class (liftIO)
import Control.Monad (join)
import qualified Data.Password.Bcrypt as Bcrypt
import qualified Data.Text.Lazy as TL
import qualified Lucid as H
import qualified Web.Scotty as S

import qualified Web.Scotty.Sqlite.Users.DbAccess as DB
import Web.Scotty.Sqlite.Users.Session
import Web.Scotty.Sqlite.Users.Model
import Web.Scotty.Sqlite.Users.Common

-----------------
-- Login Route --
-----------------

router :: StateInfo -> HtmlTemplate -> S.ScottyM ()
router sinfo template = do
  -- A Log in page
  S.get "/login" $
    loginGetAction sinfo template

  -- A request to log in
  S.post "/login" $
    loginPostAction sinfo template

  -- Logout
  S.post "/logout" $
    logoutPostAction sinfo

------

-- | A Log-in page
loginGetAction :: StateInfo -> HtmlTemplate -> S.ActionM ()
loginGetAction sinfo template = do
  mses <- getSession sinfo
  case mses of
    Nothing ->
      serveLoginForm template Nothing "" ""
    _ ->
      S.redirect "/"

-- | A request to log-in
loginPostAction :: StateInfo -> HtmlTemplate -> S.ActionM ()
loginPostAction sinfo template = do
  mses <- getSession sinfo
  case mses of
    Nothing -> do
      username <- S.param "username"
      password <- S.param "password"
      submitLoginForm sinfo template username password
    _ ->
      S.redirect "/"

-- | Logout request
logoutPostAction :: StateInfo -> S.ActionM ()
logoutPostAction sinfo = do
  mapM_ (deleteSession sinfo) =<< getSession sinfo
  S.redirect "/"

-------------------
-- Login Actions --
-------------------

-- | Returns html to welcome the user, providing links to register/login or logout.
loginOrLogout :: StateInfo -> S.ActionM Html
loginOrLogout sinfo = do
  muser <- liftIO . fmap join . traverse (DB.getLogin sinfo) =<< getSession sinfo
  pure $
    H.div_ [ H.class_ "welcome" ] $
      case muser of
        Just user ->
          mconcat
            [ "Welcome " <> H.toHtml (loginDisplayName user)
            , " "
            , H.form_ [H.method_ "post" , H.action_ "/logout", H.class_ "logout-form"] $
              H.input_ [H.type_ "submit", H.value_ "Log out", H.class_ "logout"]
            ]
        Nothing ->
          mconcat
            [ " " <> H.a_ [ H.href_ "/login" ] "Log in"
            , " | " <> H.a_ [ H.href_ "/register" ] "Register"
            ]


serveLoginForm :: HtmlTemplate -> Maybe TL.Text -> Username -> TL.Text -> S.ActionM ()
serveLoginForm template err username pass = do
  S.html $
    H.renderText $
      template
        ("Scotty.Sqlite board - register a new account")
        (loginHtml err username pass)

submitLoginForm :: StateInfo -> HtmlTemplate -> TL.Text -> TL.Text -> S.ActionM ()
submitLoginForm sinfo template username pass = do
  let
    password = Bcrypt.mkPassword (TL.toStrict pass)
  mUserId <- liftIO $ DB.checkUser sinfo username password
  case mUserId of
    Nothing ->
      serveLoginForm template (Just "Bad username or password.") username pass
    Just uid -> do
      hash <- Bcrypt.hashPassword password
      newSession sinfo uid hash
      S.redirect "/"

----------------
-- Login Html --
----------------

loginHtml :: Maybe TL.Text -> Username -> TL.Text -> Html
loginHtml err username pass = do
  H.h2_ [ H.class_ "register-title" ] "Log-in"
  H.form_
    [ H.method_ "post"
    , H.action_ "/login"
    , H.class_ "login"
    ]
    ( do
      applyMaybe err (H.p_ [ H.class_ "error" ] . H.toHtml)
      H.p_ $
        H.input_
          [ H.type_ "text"
          , H.required_ "true"
          , H.name_ "username"
          , H.placeholder_ "Username..."
          , H.value_ (TL.toStrict username)
          , H.autofocus_
          ]
      H.p_ $
        H.input_
          [ H.type_ "password"
          , H.required_ "true"
          , H.name_ "password"
          , H.placeholder_ "Password..."
          , H.value_ (TL.toStrict pass)
          ]

      H.p_ $ H.input_ [H.type_ "submit", H.value_ "Log In", H.class_ "submit-button"]
    )
