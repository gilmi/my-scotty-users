{- | Database Access.

The only relevant function here is @dbMigrations@ that should
be run in the beginning of the program.

-}

{-# LANGUAGE OverloadedStrings #-}

module Web.Scotty.Sqlite.Users.DbAccess where

import qualified Data.Text.Lazy as TL
import qualified Database.Persist as P
import qualified Database.Persist.Sql as P
import qualified Database.Persist.Sqlite as PSqlite3
import qualified Data.Password.Bcrypt as Bcrypt
import Data.Password.Bcrypt (PasswordHash(..), Password, Bcrypt, PasswordCheck(..))

import Web.Scotty.Sqlite.Users.Model
import Web.Scotty.Sqlite.Users.Common


-- ** General

-- | Run this at the start of the program to initialize the tables.
dbMigrations :: Pool -> IO ()
dbMigrations pool = P.runSqlPool (PSqlite3.runMigration migrateAll) pool

-- | Use to run a db action in IO.
runDB :: StateInfo -> P.SqlPersistT IO a -> IO a
runDB sinfo dbop = P.runSqlPool dbop (siPool sinfo)

-----------------
-- ** Users

-- | Get all users.
getAllUsers :: StateInfo -> IO Logins
getAllUsers sinfo = do
  runDB sinfo $ P.selectList [] []

-- | Get a user by their Id.
getLogin :: StateInfo -> LoginId -> IO (Maybe Login)
getLogin sinfo = runDB sinfo . P.get

-- | Get a user by their username.
getLoginByUsername :: StateInfo -> TL.Text -> IO (Maybe (P.Entity Login))
getLoginByUsername sinfo username = do
  runDB sinfo $ P.getBy (Username username)

-----------------
{- ** Login

Avoid calling these functions, they are already handled automatically via the routing actions.

-}

-- | Insert a new user into the database.
insertUser :: StateInfo -> Username -> PasswordHash Bcrypt -> Displayname -> IO (P.Key Login)
insertUser sinfo username passhash displayname = do
  runDB sinfo $ P.insert (Login username (Bcrypt.unPasswordHash passhash) displayname)

-- | Check a user credentials against the database.
checkUser :: StateInfo -> Username -> Password -> IO (Maybe (P.Key Login))
checkUser sinfo user pass = do
  list <- runDB sinfo $ do
    P.selectList [ LoginUsername P.==. user ] []
  pure $ case list of
    [P.Entity uid u] ->
      case Bcrypt.checkPassword pass (PasswordHash $ loginPasswordHash u) of
        PasswordCheckSuccess ->
          Just uid
        PasswordCheckFail ->
          Nothing
    _ -> Nothing

-- | Change the password of a user in the database.
changePassword :: StateInfo -> LoginId -> PasswordHash Bcrypt -> IO ()
changePassword sinfo uid passhash = do
  runDB sinfo $
    P.update uid
      [ LoginPasswordHash PSqlite3.=. Bcrypt.unPasswordHash passhash
      ]
