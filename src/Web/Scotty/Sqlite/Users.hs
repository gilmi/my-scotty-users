{- | My scotty users boilerplate.

This library provides user registration, authentication and session functionality.

To do this do the following things:

1. At the beginning of the program:
    - Create an sqlite database connection pool
    - Initialize the sqlite database tables with @dbMigrations@ using the connection pool
    - Initialize the session store with @initSessionStore@ and get back a SessionStore

2. Call @router@ from your routing so it will take care of the user registration, login and logout pages

3. Use @loginOrLogout@ to welcome the user and provide links for the registration, login or logout pages

4. Use @withLogin@ for @ActionM@s that require a logged-in user.

-}

{-# language OverloadedStrings #-}

module Web.Scotty.Sqlite.Users
  ( router
  , initSessionStore
  , dbMigrations
  , StateInfo(..)
  , Pool
  , SessionStore
  , Mode(..)
  , withLogin
  , withMaybeLogin
  , getSession
  , HtmlTemplate
  , Login.loginOrLogout
  , getAllUsers
  , getLogin
  , getLoginByUsername
  , module Web.Scotty.Sqlite.Users.Model
  )
where

import qualified Web.Scotty as S
import Control.Monad.IO.Class (liftIO)
import Control.Monad (join)
import qualified Database.Persist.Sql as P

import Web.Scotty.Sqlite.Users.Model
import Web.Scotty.Sqlite.Users.DbAccess
import Web.Scotty.Sqlite.Users.Common
import Web.Scotty.Sqlite.Users.Session

import qualified Web.Scotty.Sqlite.Users.Login as Login
import qualified Web.Scotty.Sqlite.Users.NewUser as NewUser
import qualified Web.Scotty.Sqlite.Users.ChangePassword as ChangePassword

-------------
-- Routing --
-------------

-- | A scotty router for login, logout and user registration
router :: StateInfo -> HtmlTemplate -> S.ScottyM ()
router sinfo template = do
  Login.router sinfo template
  NewUser.router sinfo template
  ChangePassword.router sinfo template

-- | Call with a function that expects a logged-in user.
--   Will redirect to the login page if the user is not logged-in.
withLogin :: StateInfo -> (P.Entity Login -> S.ActionM a) -> S.ActionM a
withLogin sinfo exec =
  withMaybeLogin sinfo (maybe (S.redirect "/login") exec)

-- | Call with a function that might need a logged-in user.
withMaybeLogin :: StateInfo -> (Maybe (P.Entity Login) -> S.ActionM a) -> S.ActionM a
withMaybeLogin sinfo exec = do
  getSession sinfo
    >>= liftIO . fmap join . traverse (\uid -> fmap (P.Entity uid) <$> getLogin sinfo uid)
    >>= exec
