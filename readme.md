# My scotty users boilerplate

User registration, authentication, sessions and boilerplate.

It uses:

- scotty for routing and actions
- lucid for html
- persistent and sqlite for the users database
- stm for the in memory session store
- cookie and clientsession for the cookies
- password (bcrypt) for storing password hashes

## How to use

Check the [example](/app/Main.hs).

This library provides user registration, authentication and session functionality.

To do this do the following things:

1. At the beginning of the program:
    1. Create an sqlite database connection pool. (Check out the example)
    2. Initialize the sqlite database tables with `dbMigrations` using the connection pool
    3. Initialize the session store with `initSessionStore` and get back a SessionStore

2. Call `router` from your routing so it will take care of the user registration, login and logout pages

3. Use `loginOrLogout` to welcome the user and provide links for the registration, login or logout pages

4. Use `withLogin` for `ActionM`s that require a logged-in user.

### Run the example

run:

```hs
stack run
```

and go to [http://localhost:8080](http://localhost:8080)
